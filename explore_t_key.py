#!/usr/bin/env python3

import socket
import time
import signal
from contextlib import contextmanager
import argparse

import cv2
import datetime
import time

# import datetime as dt

import os


from fire import Fire
import imutils

import urllib.request
import numpy as np

# user pass
import  base64

import getpass

import sys

import webbrowser

import math

import requests # crosson=CROSSON

from console import fg,bg,fx

# https://stackoverflow.com/questions/14494101/using-other-keys-for-the-waitkey-function-of-opencv


def main():
    IMG = "data/BEAM_OFF.jpg"
    wname = "koko"
    frame = cv2.imread( IMG)
    #cv2.namedWindow( wname , cv2.WINDOW_GUI_NORMAL   ) #

    # fs = 0 or 1 on gigavg
    lastkey = 1
    key = 1
    k2b = 0
    leshi = False
    while k2b!=113:
        cv2.imshow( wname , frame ) # 1 window for each RPi
        cv2.setWindowProperty(wname, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        cv2.setWindowProperty(wname, cv2.WND_PROP_FULLSCREEN, 0)
        cv2.resizeWindow(wname, 640 ,480 )

        #key = cv2.waitKey(1)
        lastkey = key
        key = cv2.waitKeyEx(1)
        k2b = 0xFF&key
        if key!=-1:

            if key == 1114081: leshi = True
            if key == 65505: leshi = True

            ctrlPressed = 0 != key & (1 << 18)
            if ctrlPressed: ON= fg.green
            else: ON = fg.red
            if leshi: OS= fg.green
            else: OS = fg.red

            if k2b==ord("B") or k2b==ord("b"):
                print(f"KEY={key} last={lastkey}...FF=={k2b}  ... char=={chr(k2b)}  ctrl=={ON}{ctrlPressed}{fg.default} Shift=={OS}{leshi}{fg.default}")
                print( '      You pressed %d (0x%x), 2LSB: %d (%s)' % (key, key, key % 2**16,    repr(chr(key%256)) if key%256 < 128 else '?') )
        else:
            leshi = False

        time.sleep(0.1)



if __name__=="__main__":
    Fire( main)
    #Fire({ "disp":display2,   "disp2":display2    })
