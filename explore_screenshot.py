#!/usr/bin/env python3

import pyautogui # take screenshot
import cv2
import numpy as np
from fire import Fire
import time
import os
from PIL import Image
import subprocess as sp

def shot():
    # Use gnome-screenshot to take a screenshot and save it to a file
    sp.run(['gnome-screenshot', '-f', '/tmp/screenshot.png'])
    image = Image.open('/tmp/screenshot.png')
    return image



def main():
    print()
    WAY =  os.getenv("XDG_SESSION_TYPE") == "wayland"
    # Display the image in a loop


    i = 0
    image = None
    while True:
        i += 1
        if WAY:
            if i % 20 == 0:
                image = shot()
                image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
        else:
            image = pyautogui.screenshot()
            image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)

        if image is None: continue


        scale_percent = 50
        width = int(image.shape[1] * scale_percent / 100)
        height = int(image.shape[0] * scale_percent / 100)
        # dsize
        dsize = (width, height)

        # resize image
        frame = cv2.resize(image, dsize)

        # Resize the image to 640x480
        dsize = (640, 480)
        frame = cv2.resize(frame, dsize)

        time.sleep(0.2) # from 85%cpu to 20% ??????

        cv2.imshow('Frame', frame)

        # Break the loop when 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # Release resources
    cv2.destroyAllWindows()



if __name__ == "__main__":
    Fire(main)
