#!/usr/bin/env python3

from fire import Fire
import numpy as np
import cv2
from PIL import ImageFont, ImageDraw, Image

import os
import cv2 as cv
import numpy as np
from flashcam.text_write import iprint, fonts_available

def theway2tty_cv():
    """
    strange drawing anyway
    """
    text = str(-1.234)
    font_height = 400
    thickness = 1
    # Create a blank color image
    binary_image = np.zeros((200, 400, 3), dtype=np.uint8)

    # Text parameters
    text = "Sample Text"
    text_origin = (50, 100)
    font_height = 20
    thickness = 1

    # Resolve the user's home directory and construct the absolute font path
    font_path = os.path.expanduser('~/.config/flashcam/PixelFJVerdana12pt.ttf')


    #fontname = "square_pixel-7.ttf"
    fontname = "px10.ttf"
    #fontname = "CONTF___.ttf"
    fontname = "TypographerFraktur-Medium.ttf"
    font_path = os.path.expanduser(f"~/.config/flashcam/{fontname}")

    # Create a FreeType2 object
    ft = cv.freetype.createFreeType2()

    # Load the font data
    ft.loadFontData(fontFileName=font_path, id=0)

    # Put white text on the image
    ft.putText(img=binary_image, text=text, org=text_origin, fontHeight=font_height, color=(255, 255, 255), thickness=thickness, line_type=cv.LINE_AA, bottomLeftOrigin=True)

    # Display the image
    cv.imshow('Image with Text', binary_image)
    cv.waitKey(0)
    cv.destroyAllWindows()
    #=========================================


def show(frame):
    print("i... showing")
    cv2.imshow('White Image', frame)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def empty(show = False):
    # Create a white image using a numpy array filled with 255 (white in BGR)
    white_image = np.full((480, 640, 3), 255, dtype=np.uint8)
    # Display the image
    if show:
        cv2.imshow('White Image', white_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return white_image

def pixprint( frame, fontname , fsize , n, kerning = 0):
    """
    Nay ttf font - using  fsize abd kerning
    ["pixelFJ8pt1__.TTF",8,-2] ,
    ["small_pixel.ttf",8,-1],
    """
    overtext = f" {fontname} - {fsize} - Ahoj, UPPERCASE lowercase  * pixelFJ8pt1__.TTF"
    fontpath = os.path.expanduser(f"~/.config/flashcam/{fontname}")
    #position = ( 480 , frame.shape[0]-138 ) # 480 on x
    #position = ( 10, 400 )
    position = ( 10, n )
    font = ImageFont.truetype(fontpath, fsize)
    img_pil = Image.fromarray(frame)
    draw = ImageDraw.Draw(img_pil )

    draw.fontmode = "1" # NO ANTIALIASING
    overtext = ' '.join(overtext[i:i+1] for i in range(0, len(overtext), 1))
    drtext =  str(overtext) # to be sure

    b,g,r,a = 95,0,0,0

    # Initial x and y positions
    x, y = 0, 0
    x, y = position #0, 0
    letter_spacing = kerning
    # Draw each character with custom spacing
    for char in drtext:
        draw.text((x, y), char, fill=(b,g,r,a), font=font)
        char_width, _ = draw.textsize(char, font=font)
        x += char_width + letter_spacing
    #draw.text( position,  drtext, font = font, fill = (b, g, r, a))

    frame = np.array(img_pil)
    return frame



def main():
    print()
    names = [ ["small_pixel.ttf",8,-1],
              ["small_pixel.ttf",6,0],
              ["small_pixel.ttf",5,0],
              ["prstartk.ttf",6,-3],
             ["PixelFJVerdana12pt.ttf",4,0]  ,
             ["retganon.ttf",12,-1],
             ["pixelFJ8pt1__.TTF",8,-2] ,
             ["pixelFJ8pt1__.TTF",7,-2] ,
             ["pixelFJ8pt1__.TTF",6,-2] ,
              ["TypographerFraktur-Medium.ttf",14,-2],
              ["visitor1.ttf",10,-3],
              ["visitor2.ttf",12,-3],
              ["coders_crux.ttf",16,-3],
             ["VT323-Regular.ttf",12,-2] ]

    frame = empty()

    n=10
    for i in fonts_available.keys():
        n+=25
        #frame = iprint(frame,"Ahoj: 12:34:6.33 *", "s8", ( 480 , frame.shape[0]-8 -n*10) )
        print(i)
        frame = iprint(frame,f"{i} Ahoj: 12:34:6.33 *", i, ( 480 , frame.shape[0]-8 -n ) )
        #pixprint( frame, i[0], i[1] , n, kerning=i[2])
    show(frame)

if __name__=="__main__":
    Fire(main)
