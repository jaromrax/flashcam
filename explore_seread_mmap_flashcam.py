#!/usr/bin/env python3


# this IS A TEST PROGRAM - NOT A PART OF FLASHCAM
#
#
#
#
#
print("=============  ========================")
print("=============  testbed for mmfiles  ========================")
print("=============  ========================")

import config
import mmap
import time
import os
from fire import Fire
import sys
MMAPFILE = os.path.expanduser("~/.config/flashcam/mmapfile")
MMAPSIZE = 1000

#PORT=config.CONFIG['netport']
#MMAPFILE = f"filename{PORT}"

# -------------------------------------------------------------------------
def mmcreate(PORT, filename=MMAPFILE):
    filename1 = f"{os.path.expanduser(filename)}{PORT}"
    print(f"X... creating {filename1}   ")
    with open(filename1, "w") as f:
        f.write("-"*MMAPSIZE)



def mmwrite( message , PORT, filename = MMAPFILE):
    """
    write text to filename
    """
    #PORT=config.CONFIG['netport']
    nok = True
    try:
        port = int(PORT)
        nok = False
    except:
        print("X... port must be integer number")
    if nok: sys.exit(1)
    filename1 = f"{os.path.expanduser(filename)}{PORT}"


    if not os.path.exists(filename1):
        print(f"X... {filename1} does not exist")
        mmcreate(filename)
    with open(filename1, mode="r+", encoding="utf8") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_WRITE, offset=0) as mmap_obj:
            print(f" WRITING: {message} to {filename1}")
            #if imgname.find(".jpg")<len(imgname)-5:
            #    imgname = imgname+".jpg"
            #
            #CMD = f'fixed_image "{imgname}"'
            CMD = f"{message}"
            mmap_obj.write( CMD.encode("utf8") )  # 2ms
            mmap_obj.flush()

# -------------------------------------------------------------------------

if __name__ == "__main__":
    Fire( mmwrite )
