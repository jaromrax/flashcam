#!/usr/bin/env python3
"""
 Table of a keyboard?
"""
from fire import Fire
import re
import pandas as pd

# def extract_info(s):
#     ord_values = re.findall(r'ord\("(.+?)"\)', s)
#     ctrl_status = 'not CTRL' if 'not CTRL' in s else 'CTRL' if 'CTRL' in s else None
#     function = s.split('#')[-1].strip() if '#' in s else None
#     return {'ord_values': ord_values, 'ctrl_status': ctrl_status, 'function': function}

def aextract_info(s):
    ord_values = re.findall(r'ord\("(.+?)"\)', s)
    ctrl_status = 'not CTRL' if 'not CTRL' in s else 'CTRL' if 'CTRL' in s else None
    function = s.split('#')[-1].strip() if '#' in s else None
    shift_status = ['uppercase' if char.isupper() else 'lowercase' for char in ord_values]
    return {'ord_values': ord_values, 'ctrl_status': ctrl_status, 'function': function, 'shift_status': shift_status}

def bextract_info(s):
    ord_values = re.findall(r'ord\("(.+?)"\)', s)
    ctrl_status = 'not CTRL' not in s and 'CTRL' in s
    function = s.split('#')[-1].strip() if '#' in s else None
    shift_status = [char.isupper() for char in ord_values]
    return {'ord_values': ord_values, 'ctrl_status': ctrl_status, 'function': function, 'shift_status': shift_status}


def cextract_info(s):
    ord_value_match = re.search(r'ord\("(.+?)"\)', s)
    ord_value = ord_value_match.group(1) if ord_value_match else None
    ctrl_status = 'not CTRL' not in s and 'CTRL' in s
    function = s.split('#')[-1].strip() if '#' in s else None
    shift_status = ord_value.isupper() if ord_value else False
    return {'ord_value': ord_value, 'ctrl_status': ctrl_status, 'function': function, 'shift_status': shift_status}

def extract_info(s):
    ord_value_match = re.search(r'ord\("(.+?)"\)', s)
    ord_value = ord_value_match.group(1).lower() if ord_value_match else None
    ctrl_status = 'not CTRL' not in s and 'CTRL' in s
    function = s.split('#')[-1].strip() if '#' in s else None
    shift_status = ord_value_match.group(1).isupper() if ord_value_match else False
    return {'ord_value': ord_value, 'ctrl_status': ctrl_status, 'function': function, 'shift_status': shift_status}




def main():
    print()

    FILE = "uniwrec.py"
    with open(FILE) as f:
        keyo = f.readlines()
    keyo = [ x.strip() for x in keyo if x.find("key ==") > 0 and x.find("CTRL") > 0]

    all_info = []
    for i in keyo:
        all_info.append( extract_info(i) )
    print( len(keyo) )


    # Create DataFrame
    df = pd.DataFrame(all_info)

    # Rename columns
    df.rename(columns={'ctrl_status': 'ctrl', 'shift_status': 'shift'}, inplace=True)

    # Reorder columns
    df = df[['ctrl', 'shift', 'ord_value', 'function']]


    # Sort DataFrame by ord_value
    df = df.sort_values(by='ord_value')

    # Set display option to show all rows
    pd.set_option('display.max_rows', None)

    # Display the table
    print(df)



if __name__ == "__main__":
    Fire(main)
