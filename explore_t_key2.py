#!/usr/bin/env python3
import time
from fire import Fire
from pynput.keyboard import Key, Listener
# https://stackoverflow.com/questions/58384816/how-to-detect-combination-of-key-presses-on-keyboard-in-linux-with-python-pynput

SHIFT = False
CTRL = False

def kb_on_press(key):
    global SHIFT, CTRL
    try:
        a= key.char
        #print('alphanumeric key {0} pressed'.format( key.char))
        #CTRL  = False
    except AttributeError:
        #print('special key {0} pressed'.format(  key))
        if key==Key.ctrl:
            CTRL = True
        if key==Key.shift:
            SHIFT = True

def kb_on_release(key):
    global SHIFT, CTRL
    if key==Key.ctrl: CTRL = False
    if key==Key.shift: SHIFT = False
    #print('{0} released'.format( key))
    if key == Key.esc:
        # Stop listener
        return False


def main():
    # # Collect events until released
    # with Listener(
    #         on_press=on_press,
    #         on_release=on_release) as listener:
    #     listener.join()

    # ...or, in a non-blocking fashion:
    listener = Listener(
        on_press=kb_on_press,
        on_release=kb_on_release)
    listener.start()

    while True:
        time.sleep(0.2)
        c="    "
        s="    "
        if CTRL: c="CRTL"
        if SHIFT: s="SHIFT"
        print(f" {c} x {s} ...      ", end = "       \r")
    print()

if __name__=="__main__":
    Fire(main)
