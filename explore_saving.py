#!/usr/bin/env python3
import cv2

def test(sfourcc, container_type):
    fourcc = cv2.VideoWriter_fourcc(*sfourcc)  # Change 'XVID' to desired codec

    # Define the codec and create VideoWriter object
    out = cv2.VideoWriter(f'output_{sfourcc}.{container_type}', fourcc, 20.0, (frame_width, frame_height))

    # Capture and save frames
    frame_count = 0
    while frame_count < 50:
        print(".",end="", flush=True)
        ret, frame = cap.read()
        if ret:
            out.write(frame)
            frame_count += 1
        else:
            break

    # Release everything when job is finished
    out.release()


# Set the camera resolution
frame_width = 1280
frame_height = 720
camera_port = 0  # Default camera

# Initialize video capture
cap = cv2.VideoCapture(camera_port)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)

# Set the video codec and container type
sfourcc = 'XVID'
container_type = 'avi'  # Change '.avi' to desired container type

SF = ['XVID','MJPG','DIVX', 'LAGS', 'X264', 'MP4V', 'IYUV', 'avc1', 'H264', 'V264', 'MJPG', 'MPEG', 'MP42', 'mp4v', 'CRAM', 'MSVC', 'WHAM', 'CVID', 'PIM1', 'DIV3', 'U263', 'I263', 'FLV1', 'VP09', 'VP08', 'VP90', 'VP80']
CO = ["avi","mkv","mp4"]
CO = ["mkv"]
for i in CO:
    for j in SF:
        # Set the video codec and container type
        container_type = i
        sfourcc = j
        print()
        print(i,j, end = " ")
        test(sfourcc, container_type )


cap.release()
cv2.destroyAllWindows()
