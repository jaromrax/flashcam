#!/usr/bin/env python3
#test recording of video
import cv2
import skvideo.io
import time


print("... opening")
capture=cv2.VideoCapture(0) #open the default webcam
print("... opened")
outputfile = "test.mp4"   #our output filename
writer = skvideo.io.FFmpegWriter(outputfile, outputdict={
  '-vcodec': 'libx264',  #use the h.264 codec
  '-crf': '0',           #set the constant rate factor to 0, which is lossless
  '-preset':'veryslow'   #the slower the better compression, in princple, try
                         #other options see https://trac.ffmpeg.org/wiki/Encode/H.264
})

n=0
while True:
    n+=1
    print(f"...read {n:06d}", end = "   ")
    ret,frame=capture.read()
    if ret==False:
        print("Bad frame")
        break
    cv2.imshow('display',frame)

    start = time.time()
    writer.writeFrame(frame[:,:,::-1])  #write the frame as RGB not BGR
    end = time.time()
    print("...",end - start)

    ret=cv2.waitKey(10)
    if ret==27: #esc
        break

writer.close() #close the writer
capture.release()
cv2.destroyAllWindows()
