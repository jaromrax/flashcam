#!/usr/bin/env python3

from fire import Fire
import cv2

def set_resolution(cap, width, height):
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
def main():
    # Initialize the webcam with the V4L2 backend
    cap = cv2.VideoCapture(0, cv2.CAP_V4L2)

    # Check if the camera opened successfully
    if not cap.isOpened():
        print("Error: Could not open video device.")
        return

    # Start with the resolution set to 640x480
    current_resolution = (640, 480)
    set_resolution(cap, *current_resolution)

    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        if not ret:
            print("Error: Failed to capture frame.")
            break

        # Display the resulting frame
        cv2.imshow('Webcam Feed', frame)

        # Check for key presses
        key = cv2.waitKey(1)

        # If SPACE is pressed, toggle the resolution
        if key == 32:  # 32 is the ASCII code for the SPACE key
            if current_resolution == (640, 480):
                current_resolution = (800, 600)
            else:
                current_resolution = (640, 480)
            set_resolution(cap, *current_resolution)

        # If Escape key is pressed, exit the loop
        elif key == 27:  # 27 is the ASCII code for the Escape key
            break

    # When everything is done, release the capture and close all windows
    cap.release()
    cv2.destroyAllWindows()


if __name__=="__main__":
    Fire(main)


# def main():
#     # Initialize the webcam
#     cap = cv2.VideoCapture(0)

#     # Set the resolution to 640x480
#     cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
#     cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

#     while True:
#         # Capture frame-by-frame
#         ret, frame = cap.read()

#         # Display the resulting frame
#         cv2.imshow('Webcam Feed', frame)

#         # Wait for the Escape key to be pressed to exit the loop
#         if cv2.waitKey(1) == 27:  # 27 is the ASCII code for the Escape key
#             break

#     # When everything is done, release the capture and close all windows
#     cap.release()
#     cv2.destroyAllWindows()
