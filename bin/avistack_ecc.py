#!/usr/bin/env python3

from fire import Fire

import os
import cv2
import numpy as np
from time import time



# from alive_progress import alive_bar
from alive_progress import alive_bar,config_handler

from skimage import data, util
from skimage.measure import label, regionprops


def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])


# Align and stack images with ECC method
# Slower but more accurate
def stackImagesECC(filename, total , ymargin, xmargin , real = False , corrxy = False, corrx_pol = (-3.5578e-6,0.246986,22.731), corry_pol = (1.30704e-6,0.0741977,226.59) ):
    M = np.eye(3, 3, dtype=np.float32)


    #print(corrxy)
    #return

    first_image = None
    stacked_image = None
    cap = cv2.VideoCapture(filename)
    n_image = 1

    points = []

    first_frame = True

    with alive_bar(total) as bar:
        while(cap.isOpened()):
            bar()
            ret, frame = cap.read()
            if not(ret):
                break
            # crop_img = img[y:y+h, x:x+w]
            # frame = frame[margin:-margin, margin:-margin].copy()
            frame = frame[ymargin:-ymargin, xmargin:-1].copy()
            #print(frame.shape)
            n_image+=1

            image = frame.astype(np.float32) / 255
            # image = cv2.imread(file,1).astype(np.float32) / 255

            if  (corrxy):

                if first_frame:
                    averageValue1 = np.float32(image)
                    first_frame = False

                # ----------- move as in POLY
                dwidth = corrx_pol[0]*n_image**2 +corrx_pol[1]*n_image #+corrx_pol[2]
                dheight = corry_pol[0]*n_image**2 +corry_pol[1]*n_image #+corry_pol[2]

                print(f"i... shift: {dwidth} {dheight}")
                T = np.float32([[1, 0, - dwidth], [0, 1, - dheight]])
                shifted = cv2.warpAffine(image, T, (image.shape[1], image.shape[0]))
                image = shifted


                cv2.accumulateWeighted(image, averageValue1, 1/n_image )
                #self.frame = self.averageValue1 all is white
                # image = cv2.convertScaleAbs(averageValue1)
                image = averageValue1

            elif not(real):

                img = util.img_as_ubyte( image ) > 150 # THRESHOLD
                label_img = label(img, connectivity=img.ndim)
                props = regionprops(label_img)
                # print(props[0]['centroid'] )

                rect = 15

                x,y =  props[0]['centroid'][0]  ,  props[0]['centroid'][1]
                image = cv2.rectangle(image, ( int(y -rect), int(x-rect) ),
                                      ( int(y + rect), int(x + rect) ),
                                      (36,255,12), 1)

                ROI = rgb2gray ( image[ int(y-rect):int(y+rect), int(x-rect):int(x+rect)] )

                y,x = np.nonzero(ROI)

                xmean = x.mean() - rect
                ymean = y.mean() - rect

                xmean,ymean = 0,0

                # print( xmean, ymean )

                # last tune of position
                x,y =  props[0]['centroid'][0] - xmean  ,  props[0]['centroid'][1] - ymean
                points.append( (y,x) )
                print( n_image, y, x )
                for tup in points:
                    image = cv2.circle(image, ( int(tup[0]), int(tup[1]) ),
                                       radius=0, color=(0, 0, 255), thickness=-1)

                #print(f"i...  {n_image:4d} ")

                with open("points.txt", "a") as f:
                    f.write( f"{n_image} {y:.1f} {x:.1f} \n" )


            cv2.imshow('transform_frame', image)
            if (n_image > total-2) and (n_image<total):
                image = (image*255).astype(np.uint8)
                cv2.imwrite(filename+'QQ.png', image)
                print('geeqie  '+filename+'QQ.png')
            if cv2.waitKey(2) & 0xFF == ord('q'):
                break


            if n_image>total:
                break

            if first_image is None:
                # convert to gray scale floating point image
                first_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
                stacked_image = image
            elif real:

                #try:
                #    s, M  = cv2.findTransformECC(grad1, grad2, warp_matrix, warp_mode, criteria, inputMask=None, gaussFiltSize=1)
                #except TypeError:
                #    s, M = cv2.findTransformECC(grad1, grad2, warp_matrix, warp_mode, criteria)

                ITER = 10000
                EPS = 1e-10
                criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, ITER,  EPS)

                #MOTION_TRANSLATION
                #MOTION_EUCLIDEAN  # +rotation
                #MOTION_AFFINE      #+shear
                warp_mode = cv2.MOTION_TRANSLATION
                warp_mode = cv2.MOTION_AFFINE
                warp_mode = cv2.MOTION_HOMOGRAPHY #8 pars
                warp_mode = cv2.MOTION_EUCLIDEAN
                M = np.eye(2, 3, dtype=np.float32)

                try:
                    s, M = cv2.findTransformECC(cv2.cvtColor(image,cv2.COLOR_BGR2GRAY), first_image, M, warp_mode,  criteria, inputMask=None, gaussFiltSize=1)
                except TypeError:
                    s, M = cv2.findTransformECC(cv2.cvtColor(image,cv2.COLOR_BGR2GRAY), first_image, M, warp_mode, criteria)

                # Estimate perspective transform
                #s, M = cv2.findTransformECC(cv2.cvtColor(image,cv2.COLOR_BGR2GRAY), first_image,  cv2.MOTION_HOMOGRAPHY)
                w, h, _ = image.shape
                # Align image to first image
                #image = cv2.warpPerspective(image, M, (h, w))
                # for eucl, tran, affine
                image = cv2.warpAffine(image, M, (h,w) , flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP);
                stacked_image += image

    stacked_image /= n_image
    stacked_image = (stacked_image*255).astype(np.uint8)
    isWritten = cv2.imwrite(filename+'.png', stacked_image)
    if isWritten:
        print('The image is successfully saved.', filename+'.png')

    cap.release()
    cv2.destroyAllWindows()
    return stacked_image


def main(filename, total, ymargin=25, xmargin=0 , real = False, corrxy = False):
    print("D... in main function", real)

    stackImagesECC(filename, total, ymargin=ymargin, xmargin=xmargin,
                   real=real, corrxy = corrxy)




if __name__ == "__main__":
    Fire( main )
