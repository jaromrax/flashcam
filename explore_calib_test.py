#!/usr/bin/env python3
import numpy as np
import cv2

import yaml

#load calibration matrix
calfile = 'calibration.yaml'
with open (calfile ) as file:

    documents = yaml.full_load(file)
    x=0
    for item, doc in documents.items():
        if x == 0:
            mtx = np.matrix(doc)
            x = 1
        else:
            dist = np.matrix(doc)




cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)
num = 30
found = 0
while(found < num):  # Here, 10 can be changed to whatever number you like to choose
    ret, img = cap.read() # Capture frame-by-frame
    w,h = img.shape[:2]
    if ret:

        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
        undistortedFrame = cv2.undistort(img, mtx, dist, None, newcameramtx)
        #undistortedFrame = undistortedFrame[y:y+h, x:x+w]
    cv2.imshow('img', undistortedFrame)
    cv2.waitKey(50)


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
