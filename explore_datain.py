#!/usr/bin python3
"""
simple server - print incomming data
"""

import threading
import socket

def data_acquisition():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(('localhost', 12345))
        s.listen()
        while True:
            conn, addr = s.accept()
            with conn:
                data = conn.recv(1024)
                if data:
                    process_data(data)

def process_data(data):
    # Process incoming data here
    d = data.decode('utf8')
    print(d)
    print()
    pass

acquisition_thread = threading.Thread(target=data_acquisition)
acquisition_thread.start()
