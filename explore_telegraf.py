#!/usr/bin/env python3
"""
 dont forget to create PIPE mkfifo !!!!!!!!!!!
"""
import os
import fcntl
import time

fifo_path = '/tmp/telegraf_pipe'

with open(fifo_path, 'r') as fifo:
    fd = fifo.fileno()
    # Set the file descriptor to non-blocking
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

    abc = '#'
    while True:
        abc = chr(ord(abc) + 1)
        try:
            data = fifo.readline()
            if data:
                # Process the data
                print("R... ", data.strip())
                abc = '#'
        except BlockingIOError:
            # No data available, continue the loop
            pass

        # Your loop logic here
        time.sleep(0.1)
        print(f" {abc} ", end="\r", flush=True)
        if abc > 'z': abc = '#'
